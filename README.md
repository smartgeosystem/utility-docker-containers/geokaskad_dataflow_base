# Geokaskad DataFlow Base Images

Builder and production image for Geokaskad Dataflow. Based on official alpine & golang images


## Versions

### 1.20-3.17
golang: 1.20.2  
alpine: 3.17  

### 1.22-3.19
golang: 1.22.0  
alpine: 3.19


## Packages

### Builder
 - librdkafka-dev
 - pkgconfig
 - gcc
 - musl-dev
 - git

### Prod
 - librdkafka